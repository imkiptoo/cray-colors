package scedar.co.ke.craycolors;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GamePage extends AppCompatActivity {
    RelativeLayout gameActivity;
    Point displaySize;

    TextView titleTextView, txHighScoreTitle, txHighScoreValue, txPlayerScoreTitle, txPlayerScoreValue;
    //Button playButton, shareButton;
    LinearLayout btReplayGame;

    int currentApiVersion;
    Integer score, bestScore;
    SharedPreferences sharedPreferences;

    Timer blocksTimer;
    ConstraintLayout clYouScored, clHighScore;

    Shape player;
    boolean isGameStarted;

    class Shape extends View {
        public Integer color;
        Shape(Context context) {
            super(context);
        }
    }
    private AdView mAdView;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game_page);

        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(String.valueOf(R.string.ad_unit_id));

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        gameActivity = (RelativeLayout) findViewById(R.id.activity_game);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        displaySize = new Point();
        getWindowManager().getDefaultDisplay().getSize(displaySize);
        sharedPreferences = getApplication().getSharedPreferences(getPackageName(), MODE_PRIVATE);
        bestScore = sharedPreferences.getInt("best_score", 0);

        titleTextView = (TextView) findViewById(R.id.currentScore);
        clHighScore = (ConstraintLayout) findViewById(R.id.clHighScore);
        clYouScored = (ConstraintLayout) findViewById(R.id.clYouScored);

        txHighScoreValue = (TextView) findViewById(R.id.txHighScoreValue);
        txPlayerScoreValue = (TextView) findViewById(R.id.txPlayerScoreValue);
        btReplayGame = (LinearLayout) findViewById(R.id.btReplayGame);

        player = new Shape(getApplicationContext());
        float playerSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics());
        RelativeLayout.LayoutParams playerLayoutParams = new RelativeLayout.LayoutParams((int) playerSize, (int) playerSize);
        player.setLayoutParams(playerLayoutParams);
        player.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.player));
        player.setX(displaySize.x / 2 - player.getLayoutParams().width / 2);
        player.setY(displaySize.y - displaySize.y / 4);
        gameActivity.addView(player);
        changePlayerColor();
        player.setVisibility(View.INVISIBLE);

        /*playButton = (Button) findViewById(R.id.button_play);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newGame();
            }
        });*/

        newGame();
        isGameStarted = true;

        clHighScore.setVisibility(View.INVISIBLE);
        clYouScored.setVisibility(View.INVISIBLE);
        btReplayGame.setVisibility(View.INVISIBLE);

        btReplayGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GamePage.this, GamePage.class));
                finish();
            }
        });


        currentApiVersion = android.os.Build.VERSION.SDK_INT;

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        // This work only for android 4.4+
        if(currentApiVersion >= Build.VERSION_CODES.KITKAT)
        {

            getWindow().getDecorView().setSystemUiVisibility(flags);

            // Code below is to handle presses of Volume up or Volume down.
            // Without this, after pressing volume buttons, the navigation bar will
            // show up and won't hide
            final View decorView = getWindow().getDecorView();
            decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
                    {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility)
                        {
                            if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                            {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(GamePage.this, MainActivity.class));
        finish();
    }


    @SuppressLint("NewApi")
    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        if(currentApiVersion >= Build.VERSION_CODES.KITKAT && hasFocus)
        {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    void changePlayerColor() {
        TypedArray colors = getResources().obtainTypedArray(R.array.colors);
        Integer color = colors.getColor(new Random().nextInt(colors.length()), 0);
        ((GradientDrawable) player.getBackground()).setColor(color);
        player.color = color;
        colors.recycle();
    }

    void newGame() {
        isGameStarted = true;
        score = 0;
        titleTextView.setText(String.format(Locale.getDefault(), "%d", score));
        player.setVisibility(View.VISIBLE);
        /*playButton.setVisibility(View.INVISIBLE);
        shareButton.setVisibility(View.INVISIBLE);*/
        blocksTimer = new Timer();
        blocksTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @SuppressLint("NewApi")
                    @Override
                    public void run() {
                        titleTextView.setText(String.format(Locale.getDefault(), "%d", score++));

                        Integer blockSize = displaySize.x / 4;
                        RelativeLayout.LayoutParams blockLayoutParams = new RelativeLayout.LayoutParams(blockSize, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));
                        TypedArray colors = getResources().obtainTypedArray(R.array.colors);
                        ArrayList<Integer> colorsArrayList = new ArrayList<>();
                        for (int index = 0; index < colors.length(); index++) {
                            colorsArrayList.add(colors.getColor(index, 0));
                        }
                        List animations = new ArrayList();
                        for (Integer blockCount = 0; blockCount < 4; blockCount++) {
                            final Shape block = new Shape(getApplicationContext());
                            block.setLayoutParams(blockLayoutParams);
                            block.setX(blockSize * blockCount);
                            block.setY(-block.getLayoutParams().height);
                            Integer color = colorsArrayList.get(new Random().nextInt(colorsArrayList.size()));
                            block.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.block));
                            ((GradientDrawable) block.getBackground()).setColor(color);
                            block.color = color;
                            colorsArrayList.remove(color);
                            gameActivity.addView(block);
                            final ObjectAnimator blockAnimator = ObjectAnimator.ofFloat(block, "y", displaySize.y);
                            blockAnimator.setDuration(3000);
                            blockAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                @Override
                                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                    if (new Rect((int) block.getX(), (int) block.getY(), (int) block.getX() + block.getLayoutParams().width, (int) block.getY() + block.getLayoutParams().height).intersects((int) player.getX(), (int) player.getY(), (int) player.getX() + player.getLayoutParams().width, (int) player.getY() + player.getLayoutParams().height)) {
                                        if (isGameStarted == true) {
                                            if (block.color.equals(player.color)) {
                                                gameActivity.removeView(block);
                                                blockAnimator.cancel();
                                                changePlayerColor();
                                            }
                                            else {
                                                isGameStarted = false;
                                                endGame();
                                            }
                                        }
                                    }
                                }
                            });
                            blockAnimator.addListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    gameActivity.removeView(block);
                                }
                            });
                            animations.add(blockAnimator);
                        }
                        colors.recycle();
                        AnimatorSet blocksAnimatorSet = new AnimatorSet();
                        blocksAnimatorSet.playTogether(animations);
                        blocksAnimatorSet.start();
                        //gameActivity.bringChildToFront(adView);
                    }
                });
            }
        }, 0, 1000);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN && player.getVisibility() == View.VISIBLE) {
            ObjectAnimator playerAnimator = new ObjectAnimator();
            playerAnimator.setTarget(player);
            playerAnimator.setPropertyName("x");
            playerAnimator.setDuration(100);
            if (event.getX() <= displaySize.x / 4) {
                playerAnimator.setFloatValues(((displaySize.x / 4) / 2) - player.getLayoutParams().width / 2);
            }
            if (event.getX() > displaySize.x / 4 && event.getX() <= displaySize.x / 2) {
                playerAnimator.setFloatValues((displaySize.x / 2 - (displaySize.x / 4) / 2) - player.getLayoutParams().width / 2);
            }
            if (event.getX() > displaySize.x / 2 && event.getX() <= displaySize.x / 2 + displaySize.x / 4) {
                playerAnimator.setFloatValues(displaySize.x / 2 + displaySize.x / 8 - player.getLayoutParams().width / 2);
            }
            if (event.getX() > displaySize.x / 2 + displaySize.x / 4) {
                playerAnimator.setFloatValues((displaySize.x - (displaySize.x / 4) / 2) - player.getLayoutParams().width / 2);
            }
            playerAnimator.start();
        }
        return super.onTouchEvent(event);
    }

    @SuppressLint("SetTextI18n")
    void endGame() {

        if (blocksTimer != null) {
            blocksTimer.cancel();
        }

        if (bestScore < score) {
            bestScore = score;
            sharedPreferences.edit().putInt("best_score", bestScore).apply();
        }

        titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
        titleTextView.setText("Oops don't panic!");
        txHighScoreValue.setText(String.format(Locale.getDefault(), "%d", bestScore));
        txPlayerScoreValue.setText(String.format(Locale.getDefault(), "%d", score));
        clHighScore.setVisibility(View.VISIBLE);
        clYouScored.setVisibility(View.VISIBLE);
        player.setVisibility(View.INVISIBLE);
        btReplayGame.setVisibility(View.VISIBLE);
    }
}
