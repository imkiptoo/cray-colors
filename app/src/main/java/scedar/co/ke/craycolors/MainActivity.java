package scedar.co.ke.craycolors;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Locale;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    int currentApiVersion;
    TextView txScore, txFeeling;
    Integer bestScore;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        LinearLayout llBtnPlayCircle = (LinearLayout) findViewById(R.id.llBtnPlayCircle);
        LinearLayout btnHelp = (LinearLayout) findViewById(R.id.btnHelp);
        LinearLayout btnReset = (LinearLayout) findViewById(R.id.btnReset);
        LinearLayout btnRate = (LinearLayout) findViewById(R.id.btnRate);
        LinearLayout btnCredits = (LinearLayout) findViewById(R.id.btnCredits);
        LinearLayout btnPlay = (LinearLayout) findViewById(R.id.btnPlay);

        txScore = (TextView) findViewById(R.id.txScore);
        txFeeling = (TextView) findViewById(R.id.txFeeling);

        llBtnPlayCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGamePlay();
            }
        });
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGamePlay();
            }
        });

        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openHelp();
            }
        });
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryReset();
            }
        });
        btnRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openShare();
            }
        });
        btnCredits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCredits();
            }
        });

        populateFeelings();
        showHighScore();


        currentApiVersion = android.os.Build.VERSION.SDK_INT;

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        // This work only for android 4.4+
        if(currentApiVersion >= Build.VERSION_CODES.KITKAT)
        {

            getWindow().getDecorView().setSystemUiVisibility(flags);

            // Code below is to handle presses of Volume up or Volume down.
            // Without this, after pressing volume buttons, the navigation bar will
            // show up and won't hide
            final View decorView = getWindow().getDecorView();
            decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
                    {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility)
                        {
                            if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                            {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        }
    }

    private void showHighScore() {
        sharedPreferences = getApplication().getSharedPreferences(getPackageName(), MODE_PRIVATE);
        bestScore = sharedPreferences.getInt("best_score", 0);
        txScore.setText(String.format(Locale.getDefault(), "%d", bestScore));
    }

    public void openGamePlay(){
        startActivity(new Intent(MainActivity.this, GamePage.class));
        finish();
    }
    public void openHelp(){
        startActivity(new Intent(MainActivity.this, HelpActivity.class));
    }
    public void tryReset(){
        MaterialDialog dialog = new MaterialDialog.Builder(MainActivity.this)
                .title("Reset Score?")
                .content("This will reset the high score")
                .positiveText("ok")
                .negativeText("Cancel")
                .canceledOnTouchOutside(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        sharedPreferences = getApplication().getSharedPreferences(getPackageName(), MODE_PRIVATE);
                        sharedPreferences.edit().putInt("best_score", 0).apply();
                        startActivity(new Intent(MainActivity.this, MainActivity.class));
                        finish();
                    }
                })
                .show();
    }
    public void openShare(){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = String.format(Locale.getDefault(),"Hey please check out "+ getString(R.string.app_name) +  " http://play.google.com/store/apps/details?id=" + getPackageName()+" it is an interesting android game");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }
    public void openCredits(){
        startActivity(new Intent(MainActivity.this, CreditsActivity.class));
    }

    public void populateFeelings(){
        String[] array = getResources().getStringArray(R.array.feelings);
        String randomStr = array[new Random().nextInt(array.length)];
        txFeeling.setText(randomStr);
    }

    @SuppressLint("NewApi")
    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        if(currentApiVersion >= Build.VERSION_CODES.KITKAT && hasFocus)
        {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
}
