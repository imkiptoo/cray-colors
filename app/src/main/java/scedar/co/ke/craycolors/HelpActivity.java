package scedar.co.ke.craycolors;

import android.graphics.Color;
import android.os.Bundle;

import scedar.co.ke.craycolors.help_library.Step;
import scedar.co.ke.craycolors.help_library.TutorialActivity;

public class HelpActivity extends TutorialActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addFragment(new Step.Builder().setTitle(getString(R.string.help_title_1)).setContent(getString(R.string.help_content_1)).setBackgroundColor(Color.parseColor("#2d2b53")).setDrawable(R.drawable.help_1).setSummary("").build());
        addFragment(new Step.Builder().setTitle(getString(R.string.help_title_2)).setContent(getString(R.string.help_content_2)).setBackgroundColor(Color.parseColor("#2d2b53")).setDrawable(R.drawable.help_2).setSummary("").build());
        addFragment(new Step.Builder().setTitle(getString(R.string.help_title_3)).setContent(getString(R.string.help_content_3)).setBackgroundColor(Color.parseColor("#2d2b53")).setDrawable(R.drawable.help_3).setSummary("").build());
    }
}
